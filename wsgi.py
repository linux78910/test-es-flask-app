from flask import Flask, send_from_directory
import os
import uuid
from elasticsearch import Elasticsearch

application = Flask(__name__)

es_pass = os.getenv('env_elastic', 'null')
es_host = os.getenv('env_eshost', 'null')
es_port = os.getenv('env_esport', 'null')

print(dir(Elasticsearch))

if es_host == 'null' or es_port == 'null':
	host = "http://10.0.0.50:9200"
else:
	host = "https://elastic:%s@%s:%s" % (es_pass, es_host, es_port)

print("Host: %s" % host)
print("Pass: %s" % es_pass)

es=Elasticsearch([host], verify_certs=False, timeout=60)
print(es)
if es:
    result = "true"
else:
    result = "false"

@application.route("/")
def hello():
    return send_from_directory('files','index.html')


@application.route("/insert/1")
def insert1():
    e1={
    "first_name":"nitin",
    "last_name":"panwar",
    "age": 27,
    "about": "Love to play cricket",
    "interests": ['sports','music'],
    "id" : str(uuid.uuid4())
    }

    print(e1)

    res = es.index(index='megacorp',doc_type='employee',id=1,body=e1)
    return res
    

@application.route("/insert/2")
def insert2():
    e1={
    "first_name":"julia",
    "last_name":"kumla",
    "age": 47,
    "about": "Love to play chello",
    "interests": ['reading','music'],
    "id" : str(uuid.uuid4())
    }

    print(e1)

    res = es.index(index='megacorp',doc_type='employee',id=2,body=e1)
    return res
    

@application.route("/read")
def readall():
   try:
      res=es.search(index='megacorp', body={"query": {"match_all": {}}})
      #res=es.search(index='test-data', body={"query": {"match_all": {}}})
      print("Got %d Hits:" % res['hits']['total']['value'])
    #  res=str(res['hits']['hits'])
   except Exception as ex:
           #return str(ex)
           template = "An exception of type {0} occured. Arguments:\{1!r}"
           res=template.format(type(ex).__name__, ex.args)
   return res




@application.route("/read/<id>")
def read(id):
   try:
	   res=es.get(index='megacorp',doc_type='employee',id=id)
   except Exception as ex:
           #return str(ex)
           template = "An exception of type {0} occured. Arguments:\{1!r}"
           res=template.format(type(ex).__name__, ex.args)
   return res



@application.route("/delete/<id>")
def delete(id):
   try:
	   res=es.delete(index='megacorp',doc_type='employee',id=id)
   except Exception as ex:
           #return str(ex)
           template = "An exception of type {0} occured. Arguments:\{1!r}"
           res=template.format(type(ex).__name__, ex.args)
   return res




if __name__ == "__main__":
    application.run()
